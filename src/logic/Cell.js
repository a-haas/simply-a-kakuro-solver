class Cell {

    constructor() {
        this._control = false
    }

    get control() {
        return this._control
    }

    static isClue(txt) {
        return txt.split('/').length == 2
    }

    static isBlack(txt) {
        return txt === '-'
    }
}

export default Cell