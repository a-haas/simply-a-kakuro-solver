import Black from "./Black"
import Cell from "./Cell"
import Clue from "./Clue"
import White from "./White"


class CellFactory {
    static new(txt) {
        if(Cell.isClue(txt)) {
            return new Clue(
                txt.split('/')[0], txt.split('/')[1]
            )
        }

        if(Cell.isBlack(txt)) {
            return new Black()
        } 

        return new White(0) 
    }
}

export default CellFactory