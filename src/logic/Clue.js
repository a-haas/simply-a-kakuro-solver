import Cell from "./Cell";

class Clue extends Cell {
    constructor(bottom, right) {
        super()
        this._control = true
        this._bottom = bottom
        this._right = right
        this._rightBlock = []
        this._bottomBlock = []

        this._possibleRightSum = []
        this._possibleBottomSum = []
    }

    get bottom() {
        return this._bottom
    }

    get right() {
        return this._right
    }

    get bottomBlock() {
        return this._bottomBlock
    }
    addToBottomBlock(white) {
        this._bottomBlock.push(white)
    }
    bottomBlockSum() {
        return this._bottomBlock.reduce((a, b) => a + b.value, 0)
    }

    get possibleBottomSum() {
        return this._possibleBottomSum
    }

    get rightBlock() {
        return this._rightBlock
    }
    addToRightBlock(white) {
        this._rightBlock.push(white)
    }
    rightBlockSum() {
        return this._rightBlock.reduce((a, b) => a + b.value, 0)
    }

    get possibleRightSum() {
        return this._possibleRightSum
    }

    decompose() {
        this.decompose_rec(this.right, this.rightBlock.length, 1, 9, this._possibleRightSum, [])
        this.decompose_rec(this.bottom, this.bottomBlock.length, 1, 9, this._possibleBottomSum, [])
    }

    decompose_rec(sum, k, lower, upper, arr, partial) {
        if (sum === 0 && k == 0) {
            arr.push(partial)
        }
        else if(sum > 0 && k > 0 && upper >= lower) {
            let extended = [...partial, lower];
            this.decompose_rec(
                sum - lower, k - 1, lower + 1, upper, arr, extended
            )

            this.decompose_rec(
                sum, k, lower + 1, upper, arr, partial
            )
        }
    }
}

export default Clue