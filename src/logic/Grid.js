import Black from "./Black"
import CellFactory from "./CellFactory"
import Clue from "./Clue"
import White from "./White"

class Grid {
    constructor(save) {
        this.grid = undefined
        this.txtToGrid(save)
        this.height = this.grid.length
        this.width = this.grid[0].length
    }

    txtToGrid(txt) {
        this.grid = this.generateControlStructure(txt)
        this.generateBlockStructure()
        this.decomposeClues()
    }

    gridToTxt() {
        return this.grid.reduce((accumulator, row) => {
            return accumulator + '\\n' + row.reduce((accumulator, cell) => {
                if(cell instanceof Black) {
                    return accumulator + ' -' 
                }

                if(cell instanceof Clue) {
                    return accumulator + ' ' + cell.bottom + '/' + cell.right
                }
                
                return accumulator + ' ' + cell.value
            }, '')
        }, '')
    }

    generateControlStructure(txt) {
        return txt.split(/\r?\n/).map((row) => {
            return row.split(/\s+/).map((cell) => {
                return CellFactory.new(cell)
            })
        })
    }

    generateBlockStructure() {
        for(let i = 0; i < this.grid.length; i++) {
            for(let j = 0; j < this.grid[i].length; j++) {
                if(this.grid[i][j] instanceof White) {
                    // add clue to white cells
                    this.grid[i][j].topClue = this.getTopClue(i, j)
                    this.grid[i][j].leftClue = this.getLeftClue(i, j)

                    // add cell to clue
                    this.grid[i][j].topClue.addToBottomBlock(this.grid[i][j])
                    this.grid[i][j].leftClue.addToRightBlock(this.grid[i][j])
                }
            }
        }
    }

    decomposeClues() {
        for(let i = 0; i < this.grid.length; i++) {
            for(let j = 0; j < this.grid[i].length; j++) {
                if(this.grid[i][j] instanceof Clue) {
                    this.grid[i][j].decompose()
                }
            }
        }
    }

    getTopClue(i, j) {
        while(!(this.grid[i][j] instanceof Clue)) {
            i -= 1
        }

        return this.grid[i][j]
    }

    getLeftClue(i, j) {
        while(!(this.grid[i][j] instanceof Clue)) {
            j -= 1
        }

        return this.grid[i][j]
    }
}

export default Grid