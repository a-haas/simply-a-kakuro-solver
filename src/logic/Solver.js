import Clue from "./Clue"

class Solver {
  constructor(kakuroGrid) {
    this.kakuroGrid = kakuroGrid
  }

  get grid() {
    return this.kakuroGrid.grid
  }
  
  solveKakuro(i, j) {
    const grid = this.kakuroGrid.grid
    const next = this.next(i, j)
    
    if(this.isSolved()) {
      return true
    }

    if(i >= this.kakuroGrid.height) {
      return false
    }
   
    if(grid[i][j].control) {
      return this.solveKakuro(next[0], next[1])
    }

    const possibilities = this.generatePossibilities(i, j)
    for(const n of possibilities) {
      this.kakuroGrid.grid[i][j].value = n
      if(this.solveKakuro(next[0], next[1])) {
        return true
      }
    }
    this.kakuroGrid.grid[i][j].value = 0
  
    return false
  }
  
  next(i, j) {
    if(j+1 < this.kakuroGrid.height){
      return [i, j+1] 
    }
    else {
      return [i+1, 0]
    }
  }

  generatePossibilities(i, j) {
    const currentCell = this.kakuroGrid.grid[i][j]

    const wrongValues = [...currentCell.topClue.bottomBlock , ...currentCell.leftClue.rightBlock]

    const topPossibilities = currentCell.topClue.possibleBottomSum.filter((possibility) => {
      return currentCell.topClue.bottomBlock.reduce((accumulator, c) => {
        return accumulator && (c.value === 0 || possibility.includes(c.value))
      }, true)
    })

    const leftPossibilities = currentCell.leftClue.possibleRightSum.filter((possibility) => {
      return currentCell.leftClue.rightBlock.reduce((accumulator, c) => {
        return accumulator && (c.value === 0 || possibility.includes(c.value))
      }, true)
    })

    const flattenLeftClueCombi = [... new Set([].concat.apply([], leftPossibilities))]
    const flattenTopClueCombi = [... new Set([].concat.apply([], topPossibilities))]
    const goodValues = [...new Set([...flattenLeftClueCombi, ...flattenTopClueCombi])]

    return goodValues
      .filter((v) => !wrongValues.some((item) => item.value == v))
  }
  
  isSolved() {
    let i = 0
    let j = 0
    for(i = 0; i < this.kakuroGrid.height; i++){
      for(j = 0; j < this.kakuroGrid.width; j++) {
        const cell = this.kakuroGrid.grid[i][j]
        
        if(!(cell instanceof Clue)) {
          continue
        }
        
        if(cell._bottom != cell.bottomBlockSum()) {
          return false
        }

        if(cell._right != cell.rightBlockSum()) {
          return false
        }
      }
    }
  
    return true
  }
}

export default Solver