import Cell from "./Cell";

class White extends Cell {
    constructor(value) {
        super()
        this._value = value
        this._topClue = null;
        this._leftClue = null;
    }

    get value() {
        return this._value
    }

    set value(v) {
        this._value = v
    }

    get topClue() {
        return this._topClue
    }

    set topClue(clue) {
        this._topClue = clue
    }

    get leftClue() {
        return this._leftClue
    }

    set leftClue(clue) {
        this._leftClue = clue
    }

}

export default White